import * as React from 'react';
import { KeyboardAvoidingView, View, StyleSheet, Button, TextInput, Alert, AsyncStorage } from 'react-native';
import { Constants } from 'expo';
import { Title, Card } from 'react-native-paper';


export default class Login extends React.Component {

  state = {
    login: '',
    senha: ''
  }

  _login = async () => {

    const _login = this.state.login;
    const _senha = this.state.senha;

    try {
      const login = await AsyncStorage.getItem('login');
      const senha = await AsyncStorage.getItem('senha');

      if (login && login === _login && senha && senha === _senha) {
        Alert.alert('OK', 'Logado');
        this.props.navigation.navigate('Home');
      }
      else {
        Alert.alert('Erro', 'Login ou senha invalidos');
      }
    }
    catch (e) {
      Alert.alert('Erro');
    }
  }

  _register = () => {
    this.props.navigation.navigate('Register');
  }

  render() {
    return (
      <View style={styles.container}>
        <KeyboardAvoidingView behavior="padding">

          <Card style={{ padding: 10 }}>
            <Title>Login</Title>

            <TextInput
              returnKeyType='next'
              placeholder="login"
              style={{ height: 40, borderColor: 'gray', borderWidth: 1, marginBottom: 30 }}
              onChangeText={(login) => this.setState({ login })}
              value={this.state.text}
            />
            <TextInput
              secureTextEntry
              placeholder="senha"
              style={{ height: 40, borderColor: 'gray', borderWidth: 1, marginBottom: 30 }}
              onChangeText={(senha) => this.setState({ senha })}
              value={this.state.text}
            />
            <Button title="Login" onPress={this._login} />
            <Button title="Register" onPress={this._register} />
          </Card>

        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
});