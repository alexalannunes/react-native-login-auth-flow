import firebase from 'firebase';

if (!firebase.apps.length) {
  var config = {
    apiKey: 'AIzaSyAsqW3yduHzPPkfOCUmC_mUvlKU8EXvUfw',
    authDomain: 'alexalannnunes.firebaseapp.com',
    databaseURL: 'https://alexalannnunes.firebaseio.com',
    projectId: 'alexalannnunes',
    storageBucket: 'alexalannnunes.appspot.com',
    messagingSenderId: '715929890609',
  };
  firebase.initializeApp(config);
}
export default firebase;
