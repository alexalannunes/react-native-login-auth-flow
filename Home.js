import * as React from 'react';
import { Text, View, StyleSheet, Button, TextInput, Alert, AsyncStorage } from 'react-native';
import { Constants } from 'expo';
import { Title } from 'react-native-paper';


export default class Home extends React.Component {


  _clear = async () => {
    await AsyncStorage.clear();
    this.props.navigation.replace('Login')
  }

  render() {
    return (
      <View style={styles.container}>
        <Title>Vem-vindo</Title>
        <Button title="Limpar" onPress={this._clear} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
});