import { createAppContainer, createStackNavigator } from 'react-navigation';

import Intro from './Intro';
import Register from './Register';
import Home from './Home';
import Login from './Login';

const Stack = createStackNavigator({
  Intro: {
    screen: Intro,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  Home: {
    screen: Home,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  Login: {
    screen: Login,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  Register: {
    screen: Register,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
},
  {
    initialRouteName: 'Intro',
  });

export default createAppContainer(Stack);