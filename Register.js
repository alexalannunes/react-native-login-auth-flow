import * as React from 'react';
import { KeyboardAvoidingView, View, StyleSheet, Button, TextInput, Alert, AsyncStorage } from 'react-native';
import { Constants } from 'expo';
import { Title, Card } from 'react-native-paper';


export default class Register extends React.Component {

  state = {
    login: '',
    senha: ''
  }

  _register = async () => {

    const _login = this.state.login;
    const _senha = this.state.senha;

    try {
      await AsyncStorage.setItem('login', _login);
      await AsyncStorage.setItem('senha', _senha);

      Alert.alert('OK', 'Logado');
      this.props.navigation.replace('Home');
    }
    catch (e) {
      Alert.alert('Erro');
    }
  }

  _back = () => {
    this.props.navigation.pop()
  }


  render() {
    return (
      <View style={styles.container}>

        <KeyboardAvoidingView behavior="padding">
          <Card style={{ padding: 10 }}>
            <Title>Register</Title>

            <TextInput
              returnKeyType='next'
              placeholder="login"
              style={{ height: 40, borderColor: 'gray', borderWidth: 1, marginBottom: 30 }}
              onChangeText={(login) => this.setState({ login })}
              value={this.state.text}
            />
            <TextInput
              secureTextEntry
              placeholder="senha"
              style={{ height: 40, borderColor: 'gray', borderWidth: 1, marginBottom: 30 }}
              onChangeText={(senha) => this.setState({ senha })}
              value={this.state.text}
            />
            <Button title="Register" onPress={this._register} />
            <Button title="Login" onPress={this._back} />
          </Card>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
});