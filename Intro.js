import * as React from 'react';
import { Text, View, StyleSheet, Alert, AsyncStorage, ActivityIndicator } from 'react-native';
import { Constants } from 'expo';
import { Title } from 'react-native-paper';


export default class Intro extends React.Component {

  async componentDidMount() {
    try {
      const login = await AsyncStorage.getItem('login');
      const senha = await AsyncStorage.getItem('senha');


      const route = (login && senha) ? 'Home' : 'Login';
      this.props.navigation.replace(route)

    }
    catch (e) {
      Alert.alert('Erro', JSON.stringify(e));
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#2ECC71" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
});